package compiler

import (
	"fmt"
	"github.com/alecthomas/participle/v2/lexer"
	"gitlab.com/coalang/go-compiler/parser"
	"gitlab.com/coalang/go-compiler/types"
	"strconv"
)

type Obj interface {
	SrcCtx() *types.SrcContext

	Code() string
	Eval(*Context) (Obj, error)
	Child(string) Obj
	From(parser.Thing) Obj
	fmt.Stringer
}

type Context struct {
	Ctx *types.SrcContext

	Mem *Generic
}

func NewObjs(objs *parser.Objs) []Obj {
	panic("implement NewObjs")
}

func NewObj(obj *parser.Obj) Obj {
	var re Obj
	switch {
	case obj.In.Block != nil:

	}
	return re
}

func forMap(objs []Obj) map[string]Obj {
	re := map[string]Obj{} // TODO: maybe use []Obj AND map[string]Obj inside Map
	for i, obj := range objs {
		re[strconv.FormatInt(int64(i), 10)] = obj
	}
	return re
}

func solveSuffixes(in Obj, suffixes []parser.Suffix) *Call {
	switch len(suffixes) {
	case 0:
		panic("there is a bug in the code - len(suffixes) should not be 0 since solveSuffixes should not have been called with that.")
	case 1:
		suffix := suffixes[0]
		switch {
		case suffix.Map != nil:
			return NewCall(
				in.SrcCtx(),
				in,
				NewMap(
					toSrcCtx(suffix.Pos, suffix.EndPos, suffix.Tokens),
					forMap(NewObjs(suffix.Map.Objs)),
				),
			)
		case suffix.Method != nil:
			return NewCall(
				in.SrcCtx(),
				suffix.Method.Ref,
				NewMap(
					toSrcCtx(suffix.Pos, suffix.EndPos, suffix.Tokens),
					forMap(NewObjs(suffix.Method.Map.Objs)),
				),
			)
		}
	default:
		suffix := suffixes[0]
		return NewCall(
			toSrcCtx(suffix.Pos, suffix.EndPos, suffix.Tokens),
			solveSuffixes(in, suffixes[1:]),
			suffix.Map,
		)
	}
}

//func objsToObjs(objs *parser.Objs) ([]Obj, *types.SrcContext) {
//	re := make([]Obj, 1+len(objs.In))
//	ctx := toSrcCtx(objs.Pos, objs.EndPos, objs.Tokens)
//	re[0] = objToObj(objs.Left)
//	for i, obj := range objs.In {
//		re[i] = objToObj(obj.Right)
//	}
//	return re, ctx
//}
//
//func objToObj(obj parser.Obj) Obj {
//	switch {
//	case obj.In.Block != nil:
//		if len(obj.Suffixes) == 0 {
//			objs, ctx := objsToObjs(obj.In.Block.Objs)
//			return NewBlock(
//				ctx,
//				nil,
//				objs,
//			)
//		} else {
//			return NewCall(
//				toSrcCtx(obj.Pos, obj.EndPos, obj.Tokens),
//				NewBlock(nil, nil, nil).From(obj.In.Block),
//				objsToObjs(obj.Suffixes[0].),
//			)
//		}
//	case obj.In.Text != nil:
//	case obj.In.Map != nil:
//	case obj.In.Ref != nil:
//	}
//}

func toSrcCtx(pos lexer.Position, endPos lexer.Position, tokens []lexer.Token) *types.SrcContext {
	reTokens := make([]types.Token, len(tokens))
	for i, token := range tokens {
		reTokens[i] = types.Token{
			Type:  token.Type,
			Value: token.Value,
			Pos: types.SrcPos{
				Name:   token.Pos.Filename,
				Offset: token.Pos.Offset,
				Line:   token.Pos.Line,
				Column: token.Pos.Column,
			},
		}
	}

	return &types.SrcContext{
		Pos: types.SrcPos{
			Name:   pos.Filename,
			Offset: pos.Offset,
			Line:   pos.Line,
			Column: pos.Column,
		},
		EndPos: types.SrcPos{
			Name:   endPos.Filename,
			Offset: endPos.Offset,
			Line:   endPos.Line,
			Column: endPos.Column,
		},
		Tokens: reTokens,
	}
}
