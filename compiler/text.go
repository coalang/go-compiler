package compiler

import (
	"gitlab.com/coalang/go-compiler/parser"
	"gitlab.com/coalang/go-compiler/types"
)

type Text struct {
	Ctx *types.SrcContext `json:"c"`

	Text string `json:"t"`
}

func NewText(ctx *types.SrcContext, text string) *Text {
	tmp := Text{
		Ctx:  ctx,
		Text: text,
	}

	return &tmp

}

func (t *Text) SrcCtx() *types.SrcContext {
	return t.Ctx
}

func (t *Text) Code() string {
	return t.Text
}

func (t *Text) Eval(*Context) (Obj, error) {
	return t, nil
}

func (t *Text) Child(string) Obj {
	return nil // TODO: implement calling <base>..text.upper, etc
}

func (t *Text) From(thing parser.Thing) Obj {
	text := thing.(parser.Text)
	return NewText(
		toSrcCtx(text.Pos, text.EndPos, text.Tokens),
		text.Text,
	)
}

func (t *Text) String() string {
	return t.Text[1 : len(t.Text)-1]
}
