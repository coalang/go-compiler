package base

import (
	"fmt"
	"gitlab.com/coalang/go-compiler/compiler"
	"gitlab.com/coalang/go-compiler/types"
	"gitlab.com/coalang/go-compiler/ui"

	"bufio"
	"os"
)

var nilCtxNix = compiler.NewNix(nil)

var in = compiler.NewNative(
	nil,
	nil,
	func(ctx *types.SrcContext, args *compiler.Context) (compiler.Obj, error) {
		reader := bufio.NewReader(os.Stdin)
		c, _, err := reader.ReadLine()
		if err != nil {
			panic(err)
		}
		return compiler.NewText(
			nil,
			string(c),
		), nil
	},
)

var out = compiler.NewNative(
	nil,
	nil,
	func(ctx *types.SrcContext, argsRaw *compiler.Context) (compiler.Obj, error) {
		ui.Debugf("out: %s, %s", ctx, argsRaw)
		args, kwargs := compiler.SplitMap(argsRaw.Mem.Objs)
		sep := " "
		if sepArg, ok := kwargs["sep"]; ok {
			sep = sepArg.String()
		}
		out := ""
		for _, arg := range args {
			if arg != nil {
				out += arg.String() + sep
			}
		}
		fmt.Println(out[:len(out)-1])
		return nilCtxNix, nil
	},
)

func blank(objs map[string]compiler.Obj) compiler.Obj {
	return compiler.NewGeneric(
		nil,
		objs,
	)
}

var Base = blank(map[string]compiler.Obj{
	"": blank(map[string]compiler.Obj{
		"": blank(map[string]compiler.Obj{
			"io": blank(map[string]compiler.Obj{
				"out": out,
				"in":  in,
			}),
		}),
	}),
}).(*compiler.Generic)
