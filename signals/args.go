package signals

import (
	"fmt"
	"gitlab.com/coalang/go-compiler/types"
)

type Args struct {
	Ctx *types.SrcContext

	Ref    string
	Wanted string
	Got    string
}

func (a Args) SrcCtx() *types.SrcContext {
	return a.Ctx
}

func (a Args) Msg() string {
	return fmt.Sprintf("%s did not have enough arguments; wanted %s but got %s", a.Ref, a.Wanted, a.Got)
}

func (a Args) Error() string {
	ctx := ""
	if a.Ctx != nil {
		ctx = a.Ctx.Fmt() + " "
	}
	msg := ""
	if a.Msg() != "" {
		msg = a.Msg()
	}
	return fmt.Sprintf("%s%s", ctx, msg)
}
