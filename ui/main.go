package ui

import (
	"fmt"
	"io"
	"os"
	"strings"

	"github.com/fatih/color"
)

// Log Levels
// 0 Force Log (eg legal text)
// 1 Fatal
// 2 Error
// 3 Warningf
// 4 Infof
// 5 Debug

var Writer = os.Stdout //nolint:gochecknoglobals

var Level level = 6 //nolint:gochecknoglobals

type Label struct {
	Msg1  string
	Msg2  string
	Func1 func(w io.Writer, a ...interface{}) (n int, err error)
	Func2 func(w io.Writer, a ...interface{}) (n int, err error)
}

var Labels = map[level]Label{
	0: {
		Msg1:  " force ",
		Msg2:  "     : ",
		Func1: color.New(color.FgBlack, color.BgHiMagenta).Fprint,
		Func2: color.New(color.FgHiMagenta).Fprint,
	},
	1: {
		Msg1:  " FATAL ",
		Msg2:  "     : ",
		Func1: color.New(color.FgHiWhite, color.BgHiRed).Fprint,
		Func2: color.New(color.FgHiWhite).Fprint,
	},
	2: {
		Msg1:  " ERROR ",
		Msg2:  "     : ",
		Func1: color.New(color.FgBlack, color.BgHiRed).Fprint,
		Func2: color.New(color.FgHiRed).Fprint,
	},
	3: {
		Msg1:  "  warn ",
		Msg2:  "     : ",
		Func1: color.New(color.FgBlack, color.BgHiYellow).Fprint,
		Func2: color.New(color.FgHiYellow).Fprint,
	},
	4: {
		Msg1:  "    ok ",
		Msg2:  "     : ",
		Func1: color.New(color.FgBlack, color.BgHiGreen).Fprint,
		Func2: color.New(color.FgHiGreen).Fprint,
	},
	5: {
		Msg1:  "  info ",
		Msg2:  "     : ",
		Func1: color.New(color.FgHiWhite, color.BgHiCyan).Fprint,
		Func2: color.New(color.FgHiCyan).Fprint,
	},
	6: {
		Msg1:  " debug ",
		Msg2:  "     : ",
		Func1: color.New(color.FgHiWhite, color.BgHiBlack).Fprint,
		Func2: color.New(color.FgHiWhite).Fprint,
	},
}

const LevelForce level = 0   //nolint:gochecknoglobals
const LevelFatal level = 1   //nolint:gochecknoglobals
const LevelError level = 2   //nolint:gochecknoglobals
const LevelWarning level = 3 //nolint:gochecknoglobals
const LevelOk level = 4      //nolint:gochecknoglobals
const LevelInfo level = 5    //nolint:gochecknoglobals
const LevelDebug level = 6   //nolint:gochecknoglobals

const Fallbacklevel level = 6

func TestLog() {

	Forcef("cannot be silenced; does not interrupt execution")
	Fatalf("related to exit of program; interrupts all excecution")
	Errorf("may cause fatal; does interrupt some execution")
	Warningf("non fatal warning; does not interrupt execution")
	Okf("success")
	Infof("hints or information")
	Debugf("debug information")
}

func Logf(level level, format string, a ...interface{}) {
	if Level >= level {
		label, ok := Labels[level]
		if !ok {
			Level = Fallbacklevel
			Warningf("invalid log level set, defaulted to %v", Fallbacklevel)
		}
		text := fmt.Sprintf(format, a...)
		lines := strings.Split(text, "\n")
		var err error
		for i, line := range lines {
			if !strings.HasSuffix(line, "\n") {
				line += "\n"
			}
			msg := label.Msg2
			if i == 0 {
				msg = label.Msg1
			}
			_, err = label.Func1(Writer, msg)
			if err != nil {
				panic(err)
			}
			_, err = label.Func2(Writer, " "+line)
			if err != nil {
				panic(err)
			}
		}
		//pre := strings.TrimSuffix(
		//	strings.ReplaceAll(
		//		label.Msg1 + fmt.Sprintf(format, a...),
		//		"\n",
		//		"\n" + label.Msg1,
		//	),
		//	label.Msg1,
		//)
		//if !strings.HasSuffix(pre, "\n") {
		//	pre += "\n"
		//}
		//_, err := label.Func1(
		//	Writer,
		//	pre,
		//)

		if err != nil {
			panic(err)
		}
	}
}

func Forcef(format string, a ...interface{}) {
	Logf(LevelForce, format, a...)
}

func Fatalf(format string, a ...interface{}) {
	Logf(LevelFatal, format, a...)
}

func Panicf(format string, a ...interface{}) {
	Fatalf(format, a...)
	panic(fmt.Sprintf(format, a...))
}

func Errorf(format string, a ...interface{}) {
	Logf(LevelError, format, a...)
}

func Warningf(format string, a ...interface{}) {
	Logf(LevelWarning, format, a...)
}

func Okf(format string, a ...interface{}) {
	Logf(LevelOk, format, a...)
}

func Infof(format string, a ...interface{}) {
	Logf(LevelInfo, format, a...)
}

func Debugf(format string, a ...interface{}) {
	Logf(LevelDebug, format, a...)
}
